<?php

namespace Drupal\mellencare_api\Plugin\rest\resource;

use Drupal;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\update\UpdateManagerInterface;

/**
 * Provides a Modules Resource.
 *
 * @RestResource(
 *   id = "mellencare_modules_resource",
 *   label = @Translation("Modules Resource"),
 *   uri_paths = {
 *     "canonical" = "/mellencare/api/modules"
 *   }
 * )
 */
class ModulesResource extends ResourceBase {

  /**
   * Responds to entity GET requests.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The response of the endpoint.
   */
  public function get() {
    $modules = \Drupal::service('extension.list.module')->getAllInstalledInfo();
    $available = update_get_available(TRUE);
    $updates = update_calculate_project_data($available);

    $response = [];
    foreach ($modules as $module) {
      $name = ($module['project']) ?? NULL;
      if (!empty($name)) {
        $response[$name] = [
          'name' => $name,
          'type' => $updates[$name]['project_type'],
          'version' => $updates[$name]['info']['version'],
          'recommended' => $updates[$name]['recommended'],
          'latest_version' => $updates[$name]['latest_version'],
          'is_uptodate' => $updates[$name]['status'] === UpdateManagerInterface::CURRENT,
        ];
      }
    }

    // Add core informations.
    $response['drupal'] = [
      'name' => 'drupal',
      'type' => 'core',
      'version' => Drupal::VERSION,
      'recommended' => $updates['drupal']['recommended'],
      'latest_version' => $updates['drupal']['latest_version'],
      'is_uptodate' => $updates['drupal']['status'] === UpdateManagerInterface::CURRENT,
    ];

    return new ResourceResponse($response);
  }

}
