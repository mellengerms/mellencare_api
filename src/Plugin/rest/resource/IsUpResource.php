<?php

namespace Drupal\mellencare_api\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;

/**
 * Provides a IsUp Resource.
 *
 * @RestResource(
 *   id = "mellencare_is_up_resource",
 *   label = @Translation("Is Up Resource"),
 *   uri_paths = {
 *     "canonical" = "/mellencare/api/is_up"
 *   }
 * )
 */
class IsUpResource extends ResourceBase {

  /**
   * Responds to entity GET requests.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The response of the endpoint.
   */
  public function get() {
    $response = [
      'message' => 'The website is up',
      'status' => TRUE,
    ];
    return new ResourceResponse($response);
  }

}
